var red_range = ['#ff0000', '#330000']
var blue_range = ['#CCCCFF', '#000033']
var green_range = ["#0aee06", "#022f01"]
var purple_range = ["#c708f4", "#270130"]

var states = [];

function load_states(dataset) {
	states = dataset;
	var uf = dataset.map(function(d){return d.uf;});
	
	for (var i = 0; i < dataset.length; i++) {

		$("#" + uf[i]).qtip({
			    content: {
			    	title: '',
			        text: ''
			        
			    },
			    position: {
			        my: 'top left',
			        at: 'bottom right'
			    },
			    style: {
	        		classes: 'qtip-blue qtip-bootstrap'
	    		}
		});
		var estado = dataset.filter(function(d) { return d.uf == uf[i]; });
		estado = estado.map(function(d){return d.estado;});
		
		$("#" + uf[i]).data('qtip').options.content.title = estado;
	}
}

function plotColorMap_obras(dataset, colorscale ) {
	var type_color;
	switch(colorscale){
		case 1: 
			type_color = green_range;
			break;
		case 2: 
			type_color = red_range;
			break;
		case 3: 
			type_color = blue_range;
			break;
		case 4: 
			type_color = purple_range;
			break;
		default:
			alert("Not allowed!");
			break;
	}
	var uf = dataset.map(function(d){return d.uf;});
	//var states = dataset.map(function(d){return d.estado;});
	var value = dataset.map(function(d){return d.value;});
	var div_states = d3.select("#states");
	var max = Math.max.apply(null, value);
	var min = Math.min.apply(null, value);
	var rank = dataset.map(function(d){return d.posicao;});
	

	var color = d3.scale.linear().domain([0,10]).range(type_color);
	
	for (var i = 0; i < dataset.length; i++) {
		var sub = parseInt((value[i]-min)/(max/10));
		
		var stateID = div_states.select("#" + uf[i]);
		//var state = states[i];
		var pos = rank[i];

		stateID
			.transition()
			.duration(Math.floor((Math.random()*1000)+1))		  
			.style("fill", color(sub));

		$("#"+uf[i]).data('qtip').options.content.text = 'Valor: '+value[i];

		
		if(i<5){
			d3.select(".panel-body")
				.append("p")
				.attr("class", "nome_cidade")
				.html("<span class='badge'>#"+pos+": "+uf[i]+" </span>  Total de obras: "+value[i]+"")
			;
		}

	}

}


function plotColorMap_obras2(dataset, colorscale ) {
	
	var uf = dataset.map(function(d){return d.uf;}).unique();

	var div_states = d3.select("#states");
	var value = dataset.map(function(d){return d.value;});
	var max = Math.max.apply(null, value);
	var min = Math.min.apply(null, value);
	
	var type_color;
	var legend ;

	switch(colorscale){
		case 1: 
			type_color = green_range;
			legend = "  Total de Obras: ";
			break;
		case 2: 
			type_color = red_range;
			legend = "  Obras Concluídas: ";
			break;
		case 3: 
			type_color = blue_range;
			legend = "  Obras em Execução: ";
			break;
		case 4: 
			type_color = purple_range;
			legend = "  Total de Investimento: R$";
			break;
		default:
			alert("Not allowed!");
			break;
	}

	var color = d3.scale.linear().domain([0,10]).range(type_color);
	
	for (var i = 0; i < uf.length; i++) {
		
		value = dataset.filter(function(d){return (d.uf == uf[i]);});

		var sub = parseInt((value[0].value - min)/(max/10));
		var pos = dataset.filter(function(d){return (d.uf == uf[i]);});
		var stateID = div_states.select("#" + uf[i]);

		stateID
			.transition()
			.duration(Math.floor((Math.random()*1000)+1))		  
			.style("fill", color(sub))
		;

		$("#"+uf[i]).data('qtip').options.content.text = 'Valor: '+value[0].value;

		
		if(i<5){
			d3.select(".panel-body")
				.append("p")
				.attr("class", "nome_cidade")
				.html("<span class='badge'>#"+(i+1)+": "+uf[i]+" </span>  "+legend+" "+value[0].value+"")
			;
		}
		 

	}



}

Array.prototype.unique = function() {
    var o = {}, i, l = this.length, r = [];
    for(i=0; i<l;i+=1) o[this[i]] = this[i];
    for(i in o) r.push(o[i]);
    return r;
};





