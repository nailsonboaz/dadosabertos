var rankQtd = [];
var rankInvest = [];
var dataset2 = [];
var states = [];

function getMenuOption(selection) {
	option = selection.options[selection.selectedIndex].value;
	d3.select(".panel-body").selectAll("p").remove();
	switch(option){
		case "obra":
			$("#title_ranking").text("Ranking por Quantidade de Obras:");
			$("#header_title").text("Quantidade total de Obras do PAC por Estado(2011-2013):");
			$("#div_ranking").show();
		  	plotColorMap_obras(rankQtd,1);
		  break;

		case "concluido":
			$("#title_ranking").text("Ranking por Quantidade de Obras Concluídas:");
			$("#header_title").text("Quantidade de Obras Concluídas do PAC por Estado(2011-2013):");
			$("#div_ranking").show();
		  	plotColorMap_obras(dataset2,2);
		  break;

		case "execucao":
			$("#title_ranking").text("Ranking por Quantidade de Obras em Execução:");
			$("#header_title").text("Quantidade de Obras do PAC por Estado(2011-2013):");
			$("#div_ranking").show();
		  break;

		case "investimento":
			$("#title_ranking").text("Ranking por Investimentos em Obras:");
			$("#header_title").text("Investimento total em Obras do PAC por Estado(2011-2013):");
			plotColorMap_obras2(rankInvest, 4 );
			$("#div_ranking").show();
		  	
		  break;

		default:
		  	alert("Not allowed!");
		  break;
	}
	
}



//Carrega arquivo inicial e os botoes
function loadData() {
	

	d3.csv("data/rank.qtdObras.csv" , function (data){
		rankQtd = data;
	});

	d3.csv("data/rank.investObras.csv" , function (data){
		rankInvest = data;
	});

	d3.csv("data/qtd_obras/pac_2012_12.qtd.obras.csv" , function (data){
		dataset2 = data;
	});

	

	d3.csv("data/estados.csv" , function (data){
		states = data;
		load_states(data);
	});

	//plot_barchart();

	

}



