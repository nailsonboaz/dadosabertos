
function plot_map(){ 
	var width = 960,
		height = 650,
		centered;

	var projection = d3.geo.mercator()
		.scale(5070)
		.translate([570, -220]);

	var path = d3.geo.path()
		.projection(projection);

	var svg = d3.select("#plot_area").append("svg")
		.attr("width", width)
		.attr("height", height);

	svg.append("rect")
		.attr("class", "background")
		.attr("width", width)
		.attr("height", height)
		.on("click", zoom);

	var g = svg.append("g")
		.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")")
	  .append("g")
		.attr("id", "states");

	d3.json("json/brjson.json", function(json) {
	  
	  var states = g.selectAll("path")
		.data(json.features)
		.enter().append("path")
		  .attr("d", path)
		  .attr("id", function(d) { return d.properties.UF; })
		  .attr("class", "paths")
		  .style("fill", "#83F52C")
		  .on("mouseover", mover)
		  .on("mouseout", mout)
		  .on("click", zoom);
	  
	  var labels = g.selectAll("text")
		.data(json.features)
		.enter().append("text")
		  .attr("transform", function(d) { return "translate(" + path.centroid(d) + ")"; })
		  .attr("id", function(d) { return 'label-'+d.properties.UF; })
		  .attr("dy", ".35em")
		  .on("click", zoom)
		  .text(function(d) { return d.properties.UF; });
	});

	
	
		
	function zoom(d) {
	  var x = 0,
		  y = 0,
		  k = 1;

	  if (d && centered !== d) {
		var centroid = path.centroid(d);
		x = -centroid[0];
		y = -centroid[1];
		k = 4;
		centered = d;
	  } else {
		centered = null;
	  }

	  g.selectAll("path")
		  .classed("active", centered && function(d) { return d === centered; });
	  g.selectAll("text")
		  .text(function(d) { return d.properties.UF; })
		  .classed("active",false);
		  
	  g.transition()
		  .duration(1000)
		  .attr("transform", "scale(" + k + ")translate(" + x + "," + y + ")")
		  .style("stroke-width", 0.7 / k + "px");
	}

	//Function to call when you mouseover a node
	function mover(d) {
	  var el = d3.select(this)	  
			.style("fill-opacity", 0.7)
			;
	}

	//Mouseout function
	function mout(d) { 
		var el = d3.select(this)
		   .style("fill-opacity", 1)
		  ;
	};

}
